

##abas-icons

`abas-icons` contains a set of icons used in the native ui

The `abas-icons` directory also includes imports for additional icon sets that can be loaded into your project.

Example loading icon set:

```html
<link rel="import" href="../abas-icons/abas-icons.html">
```

To use an icon from one of these sets, first prefix your `iron-icon` with the icon set name "abas", followed by a colon, ":", and then the icon id.

Example using the about icon from the set:

```html
<iron-icon icon="abas:about"></iron-icon>
```

See [iron-icon](https://www.webcomponents.org/element/PolymerElements/iron-icon) for more information about working with icons.

##build

```sh
bower install
```

##Run

```sh
polymer serve
```

